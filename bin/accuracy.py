#!/usr/bin/python3
HELP = """
#=============================================================================
#                           accuracy.py
#                           original by   : Shinsuke Mori
#                           re-written by : Song Bian
#                           Last change   : 20 May 2015
#=============================================================================

# 機  能 : 仮名漢字変換の精度の計算
#
# 使用法 : accuracy.py (変換結果ファイル) (正解ファイル)
#
# 実  例 : accuracy.py T.conv T.sent
#

#=====================================================================================
#                        END
#=====================================================================================
"""
import sys
import os
from math import log


def main():
  #---------------------------------------------------------------------------
  #                        Command line parsing
  #---------------------------------------------------------------------------
  # Print help file
  if (len(sys.argv) != 3):
    print(HELP, file=sys.stderr)
    exit(1)
  elif (sys.argv[1] == "-help"):
    print(HELP, file=sys.stderr)
    exit(1)
  else:
    try:
      # File handle for result files
      FILE1 = open(sys.argv[1], 'r')
      FILE2 = open(sys.argv[2], 'r')
    except Exception:
      print("Can't open %s or %s" % sys.argv[1], sys.argv[2], file=sys.stderr)
      exit(1)

  #-------------------------------------------------------------------------------------
  #                        main
  #-------------------------------------------------------------------------------------
  suc = 0                                         # マッチした文数
  nm1 = 0                                         # FILE1 の文字数
  nm2 = 0                                         # FILE2 の文字数
  nmm = 0                                         # マッチした文字
  line_num = 1
  for line1, line2 in zip(FILE1, FILE2):
    line1 = "".join(map(lambda x: x.split('/')[0], line1.split("\n")[0].split(" ")))
    line2 = "".join(map(lambda x: x.split('/')[0], line2.split("\n")[0].split(" ")))
    len1 = len(line1)
    len2 = len(line2)
    wlcs = WLCS(line1, line2)
    nm1 += len1
    nm2 += len2
    nmm += wlcs
    if ((len1 == wlcs) and (len2 == wlcs)):
      suc += 1
    else:
      print(str(line_num) + "\n" + line1 + "\n" + 
            line2 + "\n")
    line_num += 1

  print("Intersection/{0:s} = {1:d}/{2:d} = {3:5.2f}%".format(
        os.path.basename(sys.argv[1]), nmm, nm1, 100*nmm/nm1))
  print("Intersection/{0:s} = {1:d}/{2:d} = {3:5.2f}%".format(
        os.path.basename(sys.argv[2]), nmm, nm2, 100*nmm/nm2))
  print("Sentence Accuracy = {0:d}/{1:d} = {2:5.2f}%".format(
        suc, (line_num-1), 100*suc/(line_num-1)))
  #-------------------------------------------------------------------------------------
  #                        close
  #-------------------------------------------------------------------------------------
  FILE1.close()
  FILE2.close()
  exit(0)

#-------------------------------------------------------------------------------------
#                        WLCS
#-------------------------------------------------------------------------------------
def WLCS(str1, str2):
  len1 = len(str1)
  len2 = len(str2)

  DP = [ [0 for x in range(len2+1)] for x in range(len1+1)]

  for pos1 in range(1, len1 + 1):
    for pos2 in range(1, len2 + 1):
      if (str1[pos1-1: pos1] == str2[pos2-1: pos2]):
        DP[pos1][pos2] = DP[pos1-1][pos2-1] + 1
      else:
        DP[pos1][pos2] = max([DP[pos1-1][pos2], DP[pos1][pos2-1]])

  return DP[len1][len2]

if __name__ == '__main__' : main()

#=====================================================================================
#                        END
#=====================================================================================
