#!/usr/bin/python3
HELP = """
#=============================================================================
#                           kkc-word-1.py
#                           original by   : Shinsuke Mori
#                           re-written by : Song Bian
#                           Last change   : 19 May 2015
#=============================================================================

# 機  能 : 統計的手法による仮名漢字変換 (単語と入力記号列の組の1-gramモデル)
#
# 使用法 : ./kkc-word-1.py (TRAINING CORPUS)
#
# 実  例 : ./kkc-word-1.py ../corpus/L.wordkkci < ../corpus/T.kkci
#
# 注意点 : 変換する入力記号列は標準入力に与える
#          そのほかいっぱいある
#
# 参  考 : http://plata.ar.media.kyoto-u.ac.jp/mori/research/

#=============================================================================
#                        END
#=============================================================================
"""
import sys
from math import log

#---------------------------------------------------------------------------
#                        Global Variables
#---------------------------------------------------------------------------
def F_KKCInput():
  LATINU = [
      "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ", "Ｋ", 
      "Ｌ", "Ｍ", "Ｎ", "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ", "Ｕ", "Ｖ", 
      "Ｗ", "Ｘ", "Ｙ", "Ｚ"
      ]

  NUMBER = ["０", "１", "２", "３", "４", "５", "６", "７", "８", "９"]

  HIRAGANA = [
      "ぁ", "あ", "ぃ", "い", "ぅ", "う", "ぇ", "え", "ぉ", "お", "か", 
      "が", "き", "ぎ", "く", "ぐ", "け", "げ", "こ", "ご", "さ", "ざ", 
      "し", "じ", "す", "ず", "せ", "ぜ", "そ", "ぞ", "た", "だ", "ち", 
      "ぢ", "っ", "つ", "づ", "て", "で", "と", "ど", "な", "に", "ぬ", 
      "ね", "の", "は", "ば", "ぱ", "ひ", "び", "ぴ", "ふ", "ぶ", "ぷ", 
      "へ", "べ", "ぺ", "ほ", "ぼ", "ぽ", "ま", "み", "む", "め", "も", 
      "ゃ", "や", "ゅ", "ゆ", "ょ", "よ", "ら", "り", "る", "れ", "ろ", 
      "ゎ", "わ", "ゐ", "ゑ", "を", "ん"
      ]

  OTHERS = [
      "ヴ", "ヵ", "ヶ", "ー", "＝", "￥", "｀", "「", "」", "；", "’", 
      "、", "。", "！", "＠", "＃", "＄", "％", "＾", "＆", "＊", "（", 
      "）", "＿", "＋", "｜", "〜", "｛", "｝", "：", "”", "＜", "＞", 
      "？", "・"
      ]

  return LATINU + NUMBER + HIRAGANA + OTHERS

# Such that global variables can be changed
KKC_GLOBAL = sys.modules[__name__]
KKC_GLOBAL.BT, KKC_GLOBAL.UT = "BT", "UT"
KKC_GLOBAL.UTMAXLEN = 4
KKC_GLOBAL.KKCInput = F_KKCInput()
KKC_GLOBAL.CharLogP = log(1+len(KKCInput))
KKC_GLOBAL.FREQ = 0

def main():
  #---------------------------------------------------------------------------
  #                        Command line parsing
  #---------------------------------------------------------------------------
  # Print help file
  if (len(sys.argv) != 2):
    print(HELP, file=sys.stderr)
    exit(1)
  elif (sys.argv[1] == "-help"):
    print(HELP, file=sys.stderr)
    exit(1)
  else:
    try:
      # File handle for learning corpus
      ###### LC_file = open(sys.argv[1], 'r', encoding='eucjp')
      LC_file = open(sys.argv[1], 'r')
      # Lines for learning corpus
      LC = LC_file.readlines()
      LC_file.close()
    except Exception:
      print("Can't open %s" % sys.argv[1], file=sys.stderr)
      exit(1)

  #---------------------------------------------------------------------------
  #                        Generating language model PairFreq
  #---------------------------------------------------------------------------

  print('Parsing learning corpus ... ', file=sys.stderr, end="")
  PairFreq = {}
  PairFreq[BT] = 0
  PairFreq[UT] = 0
  for line in LC:
    # Generating pair dictionary
    pairs = line.split()
    for pair in pairs:
      if (pair in PairFreq):
        PairFreq[pair] += 1
      else:
        PairFreq[pair] = 1
    PairFreq[BT] += 1
  print('Done', file=sys.stderr)

  #---------------------------------------------------------------------------
  #                        Smoothing
  #---------------------------------------------------------------------------
  Freq = 0
  del_list = []
  for pair in PairFreq:
    if (pair != UT):
      freq = PairFreq[pair]
      Freq += freq
      # When the f(pair) is 1
      if (freq == 1):
        # Increment f(UT)
        PairFreq[UT] += freq
        # Append to delete list
        del_list.append(pair)

  # print(Freq)
  KKC_GLOBAL.FREQ = Freq
  # Delete keys with freq of 1
  for del_element in del_list:
    if (del_element in PairFreq):
      del PairFreq[del_element]


  #---------------------------------------------------------------------------
  #                        Generating Kana/Kanji Convertion Dictionary Dict
  #---------------------------------------------------------------------------
  Dict = {}
  for pair in PairFreq:
    if ((pair == BT) or (pair == UT)):
      continue
    kkci = pair.split('/')[1]
    if (kkci in Dict):
      Dict[kkci].append(pair)
    else:
      Dict[kkci]= [pair]


  #-------------------------------------------------------------------------------------
  #                        main process
  #-------------------------------------------------------------------------------------
  sent = sys.stdin.readline()
  while (sent):
    result = KKConv(sent, Dict, PairFreq)
    print(" ".join(result))
    sent = sys.stdin.readline()

# End main


#-------------------------------------------------------------------------------------
#                        KKConv
#-------------------------------------------------------------------------------------
def KKConv(sent, Dict, PairFreq):
  POSI = len(sent)

  VTable = [ [] for i in range(POSI+1)]
  VTable[0].append(["NULL", BT, 0])

  # Analysis Point (Dictionary Lookup Rightmost)
  for i_posi in range(1, POSI + 1):
    # Start Point (Dictionary Lookup Leftmost)
    for i_from in range(0, i_posi):
      # kkci = sent[2*i_from:(2*i_from + 2*(i_posi-i_from))]
      kkci = sent[i_from:(i_from + (i_posi-i_from))]
      # This condition is implied in perl
      if (kkci in Dict):
        # Loop for known language
        for pair in Dict[kkci]:
          # Initialize best node
          best = ["NULL", "NULL", 0]
          for node in VTable[i_from]:
            logP = (node[2] - log(PairFreq[pair]/FREQ))
            if ((best[1] == "NULL") or
                (logP < best[2])):
              best = [node, pair, logP]
          # End for node

          # If best node found
          if (best[1] != "NULL"):
            VTable[i_posi].append(best)
        # End for pair
      # End if kkci

      # Generate unknown language node
      if ((i_posi - i_from) <= UTMAXLEN):
        best = ["NULL", "NULL", 0]
        for node in VTable[i_from]:
          # if (len(node) < 1):
          #   continue
          logP = (node[2] - log(PairFreq[UT]/FREQ)
                  + (i_posi - i_from + 1) * CharLogP)
          if ((best[1] == "NULL") or
              (logP < best[2])):
            pair = kkci + "/" + UT
            best = [node, pair, logP]
        # End for node
        if (best[1] != "NULL"):
          VTable[i_posi].append(best)
      # End if i_posi
    # End for i_from
  # End for i_posi

  best = ["NULL", "NULL", 0]
  for node in VTable[i_posi-1]:
    logP = node[2] - log(PairFreq[BT]/FREQ)
    if ((best[1] == "NULL") or
        (logP < best[2])):
      best = [node, BT, logP]
  # End for node

  result = []
  # Traverse result in reversed order
  # Rightmost node
  node = best[0]
  while (node[0] != "NULL"):
    result.append(node[1])
    node = node[0]

  return reversed(result)

# End KKConv

if __name__ == '__main__' : main()
